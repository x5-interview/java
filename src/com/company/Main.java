package com.company;

import java.util.*;

public class Main {

    public static int nestedFactor(int x, int n) {
        x *= x;
        while (n > 0) {
            n--;
            x = nestedFactor(x, n);
        }
        return x;
    }

    public static void main(String[] args) throws InterruptedException {

        //1
        int rez = nestedFactor(2, 4);
        System.out.println(rez);

        //2
        Task<String> stringTask = new Task<String>();

        //3
        try {

            System.out.println("try");
             throw new RuntimeException("");
            System.out.println("throw");
        } catch (IllegalArgumentException ex) {
            System.out.println("catch");
             throw ex;
        } finally {
            System.out.println("finally");
        }
        System.out.println("continue");
        //4
        try {
            System.out.println("try");
             throw new IllegalArgumentException("");
            System.out.println("throw");
        } catch (Exception ex) {
            System.out.println("catch");
            throw ex;
        } finally {
            System.out.println("finally");
        }
        System.out.println("continue");
        //5
        final int[] i = {0};
        for (int j = 0; j < 10; j++)
            for (int c = 0; c < 10; c++) {
                new Thread(() -> {
                    ++i[0];
                    Thread.sleep(3L);
                    --i[0];
                }).start();
            }
        System.out.println(i[0]);

        //6
        Map<Integer, Integer> idMap = new HashMap<>();
        List<Task<String>> taskList = new ArrayList<>();
        Repository<Task> taskRepo= new Repository<Task>();     
        int i=0;
        for (Task<String> task: taskRepo){
            idMap.put(task.getId(),i++);
            taskList.add(task);
        }
        System.out.println(taskList.get(idMap.get(realTaskId)).toString());

        //7
        Repository<Task> taskRepo= new Repository<Task>();    
        List<Task> taskList=new ArrayList<>();
        //frontend
        for(int j=0;j<100500;j++){
              taskList.add(new Task());
        }
        //backend
        for (Task task: taskList)
            taskRepo.save(task);
        }

        //8
        Integer i1 = 100;
        Integer i2 = 100;
        System.out.println(i1 == i2);

        Integer i3 = new Integer(120);
        Integer i4 = new Integer(120);
        System.out.println(i3 == i4);

        Integer i5 = 200;
        Integer i6 = 200;
        System.out.println(i5 == i6);
    }

    public static class Task<T> {
        private int id;

        public Task() {
             System.out.println("?");
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
    public static class Repository<T> extends Collection<T>{
        void save(T entity){
            db.insert(entity);
            db.commit();
        }   
    }
}
